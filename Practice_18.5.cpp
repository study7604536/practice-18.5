﻿// Practice_18.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string.h>
#include <Windows.h>
using namespace std;


class Player
{

public:
	
	string name;
	int points;

};

int main()
{
	setlocale(LC_ALL, "Russian");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int NP;
	cout << "Введите количество игроков\n";
	cin >> NP;
	Player* Rick = new Player[NP];

	for (int i = 0; i < NP; ++i)
	{
		cout << "Введите имя игрока номер " << i + 1 << "\n";
		cin >> Rick[i].name;
		cout << "Введите количество очков игрока номер " << i + 1 << "\n";
		cin >> Rick[i].points;
	}
	for (int i = 0; i < NP; ++i)
	{
		for (int j = 0; j < NP-1; ++j)
		{
			if (Rick[j].points < Rick[j+1].points)
			{
				swap(Rick[j].points, Rick[j + 1].points);
				swap(Rick[j].name, Rick[j + 1].name);
			}
		}
	}

	cout << "\n";
	cout << "Результаты\n";
	cout << "\n";
	for(int i = 0; i < NP; ++i)
	{
		cout << Rick[i].name << "   " << Rick[i].points << "\n";
	}
	
	delete[] Rick;
}

	

	



	


